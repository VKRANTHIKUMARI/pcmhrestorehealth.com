import React from "react"
import "./youtube-embed.css"
const Youtube = ({ video }) => (
  <div class='embed-container'>
      <iframe src={video} title="youtube video" frameborder='0' allowfullscreen allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>
  </div>
)
export default Youtube