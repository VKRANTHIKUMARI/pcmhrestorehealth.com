---
title: Home
---

import { graphql, Link } from 'gatsby'
import Img from 'gatsby-image'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInstagram, faTwitter } from '@fortawesome/free-brands-svg-icons'
import PostLink from "../components/post-link"
import Departments from "../data/departments.yml"
import Projects from "../data/projects.yml"
import Members from "../data/members.yml"

export const pageQuery = graphql`
  query AllBlogPosts {
    site {
      siteMetadata {
        description
        title
      }
    }
    allSitePage (
      filter: { context: { frontmatter: { collection: { eq: "blog" } } } }
      sort: { order: DESC, fields: context___frontmatter___date }
    ) {
      edges {
        node {
          id
          context {
            frontmatter {
              date
              path
              title
            }
          }
        }
      }
    }
  allMembersYaml {
    edges {
      node {
        name
        image {
          childImageSharp {
            fluid (maxWidth: 150, maxHeight: 150) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
}
`

<div class="departments">
  {Departments.map((department) =>
    <div class="department" style={{'background-color': department.color}}>
      <Link to={department.page}>
        <div class="name">{ department.name }</div>
        <div class="description">{ department.description } [...]</div>
      </Link>
    </div>
   )}
</div>

<br />

## We are different

We focus on exercising courtesy and compassion with our patients because we believe that patients respond better to a physician who is empathetic to their needs

## You are at the center of the conversation

We don't believe in the plain old one-sided conversations. We listen to what you have to say and we believe in empowering you.

## Physicians who go beyond

A physician is obligated to consider more than a diseased organ, more even than the whole man - they must view the man in his world

<section id="team" class="align-center">
<h2> Our team</h2>

<div class="members-container">
{props.data.allMembersYaml.edges.map(({node}) =>
    <div class='member-container'>
      <div class="member-image">
        <Img fluid={node.image.childImageSharp.fluid} />
      </div>
      <div class="member-name">
        { node.name }
      </div>
    </div>
)}
</div>

</section>

<h2 class="align-center">Programs</h2>
<section id="projects" class="align-center projects">
  {Projects.map((project) =>
    <div class="project" style={{'background-color': project.color}}>
      <Link to={project.page}>
        <div class="name">{ project.name }</div>
        <div class="description">{ project.description } [...]</div>
      </Link>
    </div>
   )}
</section>

<section class="location align-center">
  <h2>Reach us</h2>
  <iframe
    frameborder="0"
    scrolling="no"
    marginheight="0"
    marginwidth="0"
    src="https://www.openstreetmap.org/export/embed.html?bbox=77.56932377815248%2C12.98500862259466%2C77.57754206657411%2C12.991490320479071&amp;layer=mapnik&amp;marker=12.98824949267772%2C77.57343292236328"
    style="border: 1px solid black; height: 70vh;"
  ></iframe>
  <br />
  <small>
    <a href="https://www.openstreetmap.org/?mlat=12.98825&amp;mlon=77.57343#map=17/12.98825/77.57343">
      View Larger Map
    </a>{" "}
    | <a href="https://goo.gl/maps/EuUd6JR7eDuhwPQ58">Google Maps</a>
  </small>
  <p>
    By metro: Get down at <em>Mantri Square Sampige Road</em> station on the
    green line. Walk towards Nataraj Theatre and take right onto the road opposite Karnataka Biryani Point. Look for the board "Curie Diagnostics" on the right (which is opposite Swastik Manandi Arcade).
  </p>
  <p>
    If you find it difficult to get directions please call us at +91-8971544066
  </p>
</section>

<section class="location align-center">
<h2>Social Media</h2>

Follow us on:

<a href="https://www.instagram.com/pcmhrestore/"><FontAwesomeIcon icon={faInstagram} size="5x" /></a> &nbsp;
<a href="https://twitter.com/HealthRestore"><FontAwesomeIcon icon={faTwitter} size="5x" /></a>

<br />
<br />
</section>

# Latest Blog Posts

<p>{props.data.allSitePage.edges.map(edge => <PostLink post={edge.node.context} />)}</p>
