---
title: "AVEKSHA- Reviving Home Based Care"
redirect_from:
  - /elderly-care
  - /home-health-care
---

![](../images/Aveksha_Logo.001.jpeg)

***Aveksha*** the *Home Based Primary Care Program* initiated in April 2019 was conceived to revive and redefine approach to home based care with the principles of family medicine and palliative care; attending to the entire spectrum of cradle to grave.

Initiated as a physician-pharmacist team, we are dedicated to creating a home health care tribe through training medical and allied healthcare professionals.

Our inspiration is driven from the Patient Centred Uber model where the canvas is focused mainly on the patient/s with their family in the centre and a team of medical professionals to provide quality health at the comforts of their homes.

## Our Vision
To provide optimal individualised/ personalised care plan to build a healthier and happier you.

## Our Mission

* To deliver compassionate comprehensive health care service in the confines of people’s homes and improve their quality of life by adding life to years.
* Inspire physicians to take up this form of holistic primary care as their own mission and grow through them.

## Who needs home-care?

* Patients who are unable to access the clinic (no age limit).
* Difficut Transportation.
* Requiring personalised care.

![](../images/who-needs-home-care.jpeg)

## What do we have to offer?

We at Restore Health have created a schematic multi-disciplinary team approach with a step by step assessment of the patient in need to provide optimal therapeutic, psychological support and not limiting to the point of care services, thereby trying to rule out the central cause and reduce the unnecessary pill burden all this in the confines of their comfort zone. We don’t just evaluate the need, but help patients and family promote quality of life beyond those needs. We have a team that work closely with a passion towards the home bound patient to provide life to their years.


![](../images/what-we-offer-home-care.jpeg)

## The services involve

1. Follow-up Post Discharge/Surgeries.
2. Palliative Support.
3. Reducing Poly-Pharmacy (on multiple medications).
4. Customised/Individualised Medication Plan.
5. Psychological Support.
6. Vaccination.
7. Nutritional Care Plan.
8. Improving quality of life.

## Elderly Care

There are over 120 million people over the age of 60 years in India, and is estimated to grow by 8% every 2 years. Most elderly are homebound and unable to access primary care. Majority of elderly have multiple long standing diseases and are on ploy-pharmacy, sometimes multiple specialists monitor patients for each comorbidity. This reduces optimal therapeutic benefit and lead to reduced adherence and increased drug related events (Adverse drug reaction and Drug drug reaction).

## Benefits

* Services to reduce pill-burden.
* Personalised environment comfortable for both the patient and healthcare professional.
* Providing ease of access to healthcare professionals at your doorstep.
* Reduce long waiting hours at the clinic/s.
* Reduce frequency of hospitalisation.

<form action="https://drive.google.com/file/d/1Yp_7RI6yoZPbE08hEQE_GtrN7dYMXcZl/view?usp=sharing">
<button type="submit" class="button is-info">Brochure</button>
</form>

<form action="https://forms.gle/6F4J6gN8KaF8uazM6">
<button type="submit" class="button is-info">Book Appointment Online</button>
</form>

For further information please feel free to contact us at: 
*+91-8971544066*
Or mail us on *pcmhrestorehealth12@gmail.com*
