---
title: Team
---

import { graphql, Link } from 'gatsby'
import Img from 'gatsby-image'
import Members from "../data/members.yml"

export const pageQuery = graphql`
  query teamMembers {
  allMembersYaml {
    edges {
      node {
        name
        image {
          childImageSharp {
            fluid (maxWidth: 150, maxHeight: 150) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
}
`

<section id="team" class="align-center">
<h1> Our team</h1>

<div class="members-container">
{props.data.allMembersYaml.edges.map(({node}) =>
    <div class='member-container'>
      <div class="member-image">
        <Img fluid={node.image.childImageSharp.fluid} />
      </div>
      <div class="member-name">
        { node.name }
      </div>
    </div>
)}
</div>

</section>

